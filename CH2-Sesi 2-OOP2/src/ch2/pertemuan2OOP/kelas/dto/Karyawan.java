package ch2.pertemuan2OOP.kelas.dto;

import lombok.Data;

@Data
public class Karyawan {
    Integer id;

    String name;
}
