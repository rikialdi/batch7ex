package ch2.pertemuan2OOP.kelas.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class KaryawanDTO {

    Integer id;
    String karyawanName;
}
